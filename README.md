# Small Relay PBX

A simple small relay based manual PBX,
inspired by Iskra's desks used in ex-Yugoslavian radio relay stations.

It is designed to work with the original FDM racks,
but only with 2-wire phone connections.

## Configuration

Line (Lin) modules are configured by soldering the on board jumpers.

On the upper inside corner (looking when a module is mounted) is an
additional jumper which suggests the module current configuration.

### Lin

| Jumpers  | Phone  | FDM    |
|----------|--------|--------|
| JP1, JP2 | Closed | Open   |
| JP3, JP4 | 2-3    | 1-2    |
| JP5      | Open   | Closed |
| JP6      | Closed | Open   |

### Oper-Lin

| Jumpers            | Phone  | FDM    | Operator |
|--------------------|--------|--------|----------| 
| JP1, JP7, JP8, JP9 | 1-2    | 1-2    | 2-3      |
| JP2, JP3           | Closed | Open   | Closed   |
| JP4                | Closed | Open   | Open     |
| JP5, JP6           | 2-3    | 1-2    | 2-3      |


## Usage

### Phone calling operator

When a phone goes off hook, the operator bell will ring
and the caller red LED will turn on.
Operator answers by going off hook and selecting the caller.

Note that operator can mute the audible ringing signal,
but it will still be visible as a big red light.

### FDM calling operator

It's the same as when a phone is calling the operator,
except a ring signal is used instead a phone going off hook.

### Operator calling a phone or FDM line

Operator goes off hook, selects the required line,
and then presses or holds the ring button.

### Ending a call

When all phones hang up (including the operator),
all lines will be automatically deselected.

FDM lines are therefore dependent on at least one phone off hook.


## Problems

There were three big ringing related mistakes that require some patches.
The schematics are up to date, but the PCBs and the gerbers are not.

With the required changes the new design wouldn't be able to share the same
design for telephone and FDM.

### Biasing affected by ring signal

Relays KH1 and KH2 will be affected by the high voltage ringing signal
and will also attenuate the ringing signal too much.
The implemented patch is to supply those relays only when not ringing.
Note that the patch is different for operator and other phone lines.

For v2 there should be an additional ringing relay on each Lin board which
switches the phone connection (J1) either to the high voltage ringing signal
or to the biasing relays.
With that fix it would also be possible to individually disable ringing if
a phone goes off hook.

Switching to 48V supply (instead of 24V) may also fix the problem without much
changes, but that needs more research.

### Ringing prevents operator from answering

The second problem was that when operator phone was ringing,
it was incapable of answering.
Therefore a ringing interrupter (pulser) was added as a patch.

### Ring signal can't be detected

And the last problem was detecting bell signal from FDM because the used relay
can't get enough power behind 4u7 capacitors.

V2 should either take that impedance into account (sensitive enough relays
for that purpose are expensive) or have the detector connected directly to the
phone line while it's not selected.
