EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7050 4050 7050 4150
$Comp
L power:GND #PWR038
U 1 1 5F7FCA43
P 7050 4150
F 0 "#PWR038" H 7050 3900 50  0001 C CNN
F 1 "GND" H 7055 3977 50  0000 C CNN
F 2 "" H 7050 4150 50  0001 C CNN
F 3 "" H 7050 4150 50  0001 C CNN
	1    7050 4150
	1    0    0    -1  
$EndComp
Connection ~ 7050 3650
Wire Wire Line
	7050 3750 7050 3650
$Comp
L Device:D D5
U 1 1 5F7FB8FE
P 7050 3900
F 0 "D5" V 7004 3980 50  0000 L CNN
F 1 "1N4148" V 7095 3980 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 7050 3900 50  0001 C CNN
F 3 "~" H 7050 3900 50  0001 C CNN
	1    7050 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	7050 3650 7050 3550
Wire Wire Line
	8050 3650 7700 3650
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 5F7FA861
P 8050 3400
F 0 "J5" H 8300 3350 50  0000 C CNN
F 1 "Conn_01x01" H 8350 3450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 8050 3400 50  0001 C CNN
F 3 "~" H 8050 3400 50  0001 C CNN
	1    8050 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 2500 7050 3150
Wire Wire Line
	5150 3250 5250 3250
Connection ~ 5150 3250
Wire Wire Line
	5150 2950 5150 3250
Wire Wire Line
	5700 2950 5150 2950
Wire Wire Line
	6300 2950 6300 3350
Wire Wire Line
	6000 2950 6300 2950
$Comp
L Device:R R17
U 1 1 5F7F56E0
P 5850 2950
F 0 "R17" V 5643 2950 50  0000 C CNN
F 1 "120k" V 5734 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5780 2950 50  0001 C CNN
F 3 "~" H 5850 2950 50  0001 C CNN
	1    5850 2950
	0    1    1    0   
$EndComp
Connection ~ 5050 3450
Wire Wire Line
	5050 2900 5050 3450
Wire Wire Line
	4600 3250 5150 3250
Wire Wire Line
	4600 3800 4600 3700
$Comp
L power:GND #PWR032
U 1 1 5F7F4128
P 4600 3800
F 0 "#PWR032" H 4600 3550 50  0001 C CNN
F 1 "GND" H 4605 3627 50  0000 C CNN
F 2 "" H 4600 3800 50  0001 C CNN
F 3 "" H 4600 3800 50  0001 C CNN
	1    4600 3800
	1    0    0    -1  
$EndComp
Connection ~ 4600 3250
Wire Wire Line
	4600 3250 4600 3400
Wire Wire Line
	4600 3250 4600 3100
Wire Wire Line
	4600 2500 4600 2800
Wire Wire Line
	5050 2500 5050 2600
$Comp
L Device:D D3
U 1 1 5F7EECDF
P 5050 2750
F 0 "D3" V 5004 2830 50  0000 L CNN
F 1 "1N4148" V 5095 2830 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5050 2750 50  0001 C CNN
F 3 "~" H 5050 2750 50  0001 C CNN
	1    5050 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 5F7EEA23
P 4600 2950
F 0 "R15" H 4530 2904 50  0000 R CNN
F 1 "120k" H 4530 2995 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4530 2950 50  0001 C CNN
F 3 "~" H 4600 2950 50  0001 C CNN
	1    4600 2950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R16
U 1 1 5F7EDADE
P 4600 3550
F 0 "R16" H 4530 3504 50  0000 R CNN
F 1 "120k" H 4530 3595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4530 3550 50  0001 C CNN
F 3 "~" H 4600 3550 50  0001 C CNN
	1    4600 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R18
U 1 1 5F7E84BF
P 5850 4100
F 0 "R18" V 5643 4100 50  0000 C CNN
F 1 "120k" V 5734 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5780 4100 50  0001 C CNN
F 3 "~" H 5850 4100 50  0001 C CNN
	1    5850 4100
	0    1    1    0   
$EndComp
Connection ~ 6300 4100
Wire Wire Line
	6300 4450 6300 4100
Wire Wire Line
	6200 4450 6300 4450
$Comp
L Device:R R19
U 1 1 5F7EA7FE
P 6050 4450
F 0 "R19" V 5843 4450 50  0000 C CNN
F 1 "120k" V 5934 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5980 4450 50  0001 C CNN
F 3 "~" H 6050 4450 50  0001 C CNN
	1    6050 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 4100 6300 4100
Wire Wire Line
	5800 4450 5900 4450
Wire Wire Line
	6300 3350 5850 3350
Connection ~ 6300 3350
Wire Wire Line
	6300 4100 6300 3350
Wire Wire Line
	5050 4600 5050 4500
$Comp
L power:GND #PWR034
U 1 1 5F7E96FF
P 5050 4600
F 0 "#PWR034" H 5050 4350 50  0001 C CNN
F 1 "GND" H 5055 4427 50  0000 C CNN
F 2 "" H 5050 4600 50  0001 C CNN
F 3 "" H 5050 4600 50  0001 C CNN
	1    5050 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4100 5050 4100
Connection ~ 5400 4100
Wire Wire Line
	5400 4450 5400 4100
Wire Wire Line
	5500 4450 5400 4450
$Comp
L Device:D D4
U 1 1 5F7E8DDB
P 5650 4450
F 0 "D4" H 5650 4667 50  0000 C CNN
F 1 "1N4148" H 5650 4576 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5650 4450 50  0001 C CNN
F 3 "~" H 5650 4450 50  0001 C CNN
	1    5650 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4100 5050 4200
Connection ~ 5050 4100
Wire Wire Line
	5700 4100 5400 4100
Wire Wire Line
	5050 3450 5050 4100
Wire Wire Line
	5250 3450 5050 3450
$Comp
L Device:CP C10
U 1 1 5F7E7B4D
P 5050 4350
F 0 "C10" H 5168 4396 50  0000 L CNN
F 1 "33u" H 5168 4305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 5088 4200 50  0001 C CNN
F 3 "~" H 5050 4350 50  0001 C CNN
	1    5050 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3650 5450 3750
$Comp
L power:GND #PWR036
U 1 1 5F7E7353
P 5450 3750
F 0 "#PWR036" H 5450 3500 50  0001 C CNN
F 1 "GND" H 5455 3577 50  0000 C CNN
F 2 "" H 5450 3750 50  0001 C CNN
F 3 "" H 5450 3750 50  0001 C CNN
	1    5450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2500 5450 3050
Wire Wire Line
	4100 2900 4100 3000
Wire Wire Line
	3900 2900 4100 2900
$Comp
L power:GND #PWR030
U 1 1 5F7E6ADF
P 4100 3000
F 0 "#PWR030" H 4100 2750 50  0001 C CNN
F 1 "GND" H 4105 2827 50  0000 C CNN
F 2 "" H 4100 3000 50  0001 C CNN
F 3 "" H 4100 3000 50  0001 C CNN
	1    4100 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5F7E459F
P 3700 2900
F 0 "J4" H 3950 2850 50  0000 C CNN
F 1 "Conn_01x01" H 4000 2950 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 3700 2900 50  0001 C CNN
F 3 "~" H 3700 2900 50  0001 C CNN
	1    3700 2900
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 5F7E3F07
P 3700 2500
F 0 "J3" H 3950 2450 50  0000 C CNN
F 1 "Conn_01x01" H 4000 2550 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 3700 2500 50  0001 C CNN
F 3 "~" H 3700 2500 50  0001 C CNN
	1    3700 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6750 3350 6300 3350
$Comp
L Device:Q_NPN_EBC Q1
U 1 1 5F7E2F4F
P 6950 3350
F 0 "Q1" H 7140 3396 50  0000 L CNN
F 1 "2N3906" H 7140 3305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline_Wide" H 7150 3450 50  0001 C CNN
F 3 "~" H 6950 3350 50  0001 C CNN
	1    6950 3350
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM741 U3
U 1 1 5F7E217D
P 5550 3350
F 0 "U3" H 5894 3396 50  0000 L CNN
F 1 "LM741" H 5894 3305 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 5600 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 5700 3500 50  0001 C CNN
	1    5550 3350
	1    0    0    -1  
$EndComp
Text HLabel 8050 3650 2    50   Output ~ 0
RING_PULSE
Wire Wire Line
	7850 3400 7700 3400
Wire Wire Line
	7700 3400 7700 3650
Connection ~ 7700 3650
Wire Wire Line
	7700 3650 7050 3650
Text HLabel 4000 2650 0    50   Input ~ 0
RING_EN
Wire Wire Line
	3900 2500 4100 2500
Connection ~ 4600 2500
Wire Wire Line
	4600 2500 5050 2500
Connection ~ 5050 2500
Wire Wire Line
	5050 2500 5450 2500
Connection ~ 5450 2500
Wire Wire Line
	5450 2500 7050 2500
Wire Wire Line
	4000 2650 4100 2650
Wire Wire Line
	4100 2650 4100 2500
Connection ~ 4100 2500
Wire Wire Line
	4100 2500 4600 2500
$EndSCHEMATC
