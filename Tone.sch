EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR010
U 1 1 5F54FAA3
P 9400 1250
F 0 "#PWR010" H 9400 1000 50  0001 C CNN
F 1 "GND" V 9405 1122 50  0000 R CNN
F 2 "" H 9400 1250 50  0001 C CNN
F 3 "" H 9400 1250 50  0001 C CNN
	1    9400 1250
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5F5D2123
P 2500 3800
F 0 "R1" V 2293 3800 50  0000 C CNN
F 1 "680" V 2384 3800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2430 3800 50  0001 C CNN
F 3 "~" H 2500 3800 50  0001 C CNN
	1    2500 3800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5F623FC9
P 2150 3900
F 0 "#PWR01" H 2150 3650 50  0001 C CNN
F 1 "GND" H 2155 3727 50  0000 C CNN
F 2 "" H 2150 3900 50  0001 C CNN
F 3 "" H 2150 3900 50  0001 C CNN
	1    2150 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 1650 9050 1650
Text Label 9050 1650 0    50   ~ 0
ANY_ACT
Wire Wire Line
	9050 1850 9600 1850
Text Label 9050 1850 0    50   ~ 0
ANY_BELL_RX
Wire Wire Line
	10650 1050 10100 1050
Wire Wire Line
	9050 1050 9600 1050
Text Label 10650 1050 2    50   ~ 0
TRUNK_A
Text Label 9050 1050 0    50   ~ 0
TRUNK_B
$Comp
L Device:CP C1
U 1 1 5F6E6F5F
P 2850 3550
F 0 "C1" H 2968 3596 50  0000 L CNN
F 1 "470u" H 2968 3505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D12.5mm_P5.00mm" H 2888 3400 50  0001 C CNN
F 3 "~" H 2850 3550 50  0001 C CNN
	1    2850 3550
	1    0    0    -1  
$EndComp
Text Label 9050 1750 0    50   ~ 0
ANY_OFFHK
Text Label 9050 1150 0    50   ~ 0
EN_OPER
Text Label 9050 1950 0    50   ~ 0
TONE_A
Text Label 10650 1950 2    50   ~ 0
TONE_B
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5FA97194
P 6100 7550
F 0 "H2" H 6000 7507 50  0000 R CNN
F 1 "MountingHole_Pad" H 6000 7598 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 6100 7550 50  0001 C CNN
F 3 "~" H 6100 7550 50  0001 C CNN
	1    6100 7550
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR07
U 1 1 5FA9963E
P 6100 7350
F 0 "#PWR07" H 6100 7200 50  0001 C CNN
F 1 "+24V" H 6115 7523 50  0000 C CNN
F 2 "" H 6100 7350 50  0001 C CNN
F 3 "" H 6100 7350 50  0001 C CNN
	1    6100 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 7350 6100 7450
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FA9EAE0
P 5200 7550
F 0 "H1" H 5100 7507 50  0000 R CNN
F 1 "MountingHole_Pad" H 5100 7598 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 5200 7550 50  0001 C CNN
F 3 "~" H 5200 7550 50  0001 C CNN
	1    5200 7550
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR04
U 1 1 5FA9EAEA
P 5200 7350
F 0 "#PWR04" H 5200 7200 50  0001 C CNN
F 1 "+24V" H 5215 7523 50  0000 C CNN
F 2 "" H 5200 7350 50  0001 C CNN
F 3 "" H 5200 7350 50  0001 C CNN
	1    5200 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 7350 5200 7450
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J1
U 1 1 5FBAA9C8
P 9800 1450
F 0 "J1" H 9850 2067 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 9850 1976 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x10_P2.54mm_Horizontal" H 9800 1450 50  0001 C CNN
F 3 "~" H 9800 1450 50  0001 C CNN
	1    9800 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 1150 9050 1150
$Comp
L power:GND #PWR012
U 1 1 5FE2363F
P 10300 1250
F 0 "#PWR012" H 10300 1000 50  0001 C CNN
F 1 "GND" V 10305 1122 50  0000 R CNN
F 2 "" H 10300 1250 50  0001 C CNN
F 3 "" H 10300 1250 50  0001 C CNN
	1    10300 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10300 1250 10200 1250
Wire Wire Line
	9600 1250 9400 1250
Wire Wire Line
	9050 1950 9600 1950
Wire Wire Line
	10100 1950 10650 1950
Wire Wire Line
	9050 1750 9600 1750
Text Label 9050 1550 0    50   ~ 0
EN_SEL
Wire Wire Line
	9050 1550 9600 1550
Wire Wire Line
	9600 1350 9500 1350
Wire Wire Line
	9600 1450 9500 1450
Wire Wire Line
	9500 1450 9500 1350
Connection ~ 9500 1350
Wire Wire Line
	9500 1350 9400 1350
Wire Wire Line
	10100 1450 10200 1450
Wire Wire Line
	10200 1450 10200 1350
Wire Wire Line
	10100 1350 10200 1350
Connection ~ 10200 1350
Wire Wire Line
	10200 1350 10300 1350
$Comp
L power:+24V #PWR013
U 1 1 5FF33216
P 10300 1350
F 0 "#PWR013" H 10300 1200 50  0001 C CNN
F 1 "+24V" V 10315 1478 50  0000 L CNN
F 2 "" H 10300 1350 50  0001 C CNN
F 3 "" H 10300 1350 50  0001 C CNN
	1    10300 1350
	0    1    1    0   
$EndComp
$Comp
L power:+24V #PWR011
U 1 1 5FF3A423
P 9400 1350
F 0 "#PWR011" H 9400 1200 50  0001 C CNN
F 1 "+24V" V 9415 1478 50  0000 L CNN
F 2 "" H 9400 1350 50  0001 C CNN
F 3 "" H 9400 1350 50  0001 C CNN
	1    9400 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10100 1150 10200 1150
Wire Wire Line
	10200 1150 10200 1250
Connection ~ 10200 1250
Wire Wire Line
	10200 1250 10100 1250
$Sheet
S 3700 2900 1150 550 
U 5F6CCA99
F0 "440Hz Oscillator" 50
F1 "Tone-Osc440Hz.sch" 50
F2 "tone" O R 4850 3050 50 
F3 "osc_bias" O R 4850 3300 50 
$EndSheet
$Comp
L power:GND2 #PWR03
U 1 1 5F6E9D90
P 2850 3900
F 0 "#PWR03" H 2850 3650 50  0001 C CNN
F 1 "GND2" H 2855 3727 50  0000 C CNN
F 2 "" H 2850 3900 50  0001 C CNN
F 3 "" H 2850 3900 50  0001 C CNN
	1    2850 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR02
U 1 1 5F6EDD99
P 2850 3300
F 0 "#PWR02" H 2850 3150 50  0001 C CNN
F 1 "+24V" H 2865 3473 50  0000 C CNN
F 2 "" H 2850 3300 50  0001 C CNN
F 3 "" H 2850 3300 50  0001 C CNN
	1    2850 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3300 2850 3400
Wire Wire Line
	2850 3700 2850 3800
Wire Wire Line
	2650 3800 2850 3800
Connection ~ 2850 3800
Wire Wire Line
	2850 3800 2850 3900
Wire Wire Line
	2350 3800 2150 3800
Wire Wire Line
	2150 3800 2150 3900
$Sheet
S 3700 3900 1150 550 
U 5F6F28EC
F0 "480Hz Oscillator" 50
F1 "Tone-Osc480Hz.sch" 50
F2 "tone" O R 4850 4300 50 
F3 "osc_bias" I R 4850 4050 50 
$EndSheet
Wire Wire Line
	4850 4050 4950 4050
Wire Wire Line
	4950 4050 4950 3300
Wire Wire Line
	4950 3300 4850 3300
Wire Wire Line
	4850 3050 5150 3050
Wire Wire Line
	5150 3050 5150 4300
Wire Wire Line
	5150 4300 4850 4300
$Comp
L Device:R R2
U 1 1 5F6FE408
P 5650 3600
F 0 "R2" V 5443 3600 50  0000 C CNN
F 1 "2.7k" V 5534 3600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5580 3600 50  0001 C CNN
F 3 "~" H 5650 3600 50  0001 C CNN
	1    5650 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 4300 5150 4300
Connection ~ 5150 4300
$Comp
L power:GND #PWR06
U 1 1 5F7029AC
P 5650 4600
F 0 "#PWR06" H 5650 4350 50  0001 C CNN
F 1 "GND" H 5655 4427 50  0000 C CNN
F 2 "" H 5650 4600 50  0001 C CNN
F 3 "" H 5650 4600 50  0001 C CNN
	1    5650 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4600 5650 4500
Wire Wire Line
	5650 3750 5650 4000
Wire Wire Line
	5650 4000 6300 4000
Connection ~ 5650 4000
Wire Wire Line
	5650 4000 5650 4100
$Comp
L power:+24V #PWR05
U 1 1 5F707440
P 5650 3300
F 0 "#PWR05" H 5650 3150 50  0001 C CNN
F 1 "+24V" H 5665 3473 50  0000 C CNN
F 2 "" H 5650 3300 50  0001 C CNN
F 3 "" H 5650 3300 50  0001 C CNN
	1    5650 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3300 5650 3450
$Comp
L Transformer_XICON:42TL TR1
U 1 1 5F674B01
P 7050 3800
F 0 "TR1" H 7050 4133 50  0000 C CNN
F 1 "42TL" H 7050 4224 50  0000 C CNN
F 2 "Transformer_XICON:42TL" H 7050 4100 50  0001 C CNN
F 3 "" H 7050 4100 50  0001 C CNN
	1    7050 3800
	1    0    0    1   
$EndComp
$Comp
L power:+24V #PWR09
U 1 1 5F676733
P 7050 3300
F 0 "#PWR09" H 7050 3150 50  0001 C CNN
F 1 "+24V" H 7065 3473 50  0000 C CNN
F 2 "" H 7050 3300 50  0001 C CNN
F 3 "" H 7050 3300 50  0001 C CNN
	1    7050 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR08
U 1 1 5F676B86
P 6750 3300
F 0 "#PWR08" H 6750 3150 50  0001 C CNN
F 1 "+24V" H 6765 3473 50  0000 C CNN
F 2 "" H 6750 3300 50  0001 C CNN
F 3 "" H 6750 3300 50  0001 C CNN
	1    6750 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 3600 6750 3600
Wire Wire Line
	6750 3600 6750 3300
Wire Wire Line
	7050 3300 7050 3400
$Comp
L Device:C C2
U 1 1 5F67B713
P 6450 4000
F 0 "C2" V 6198 4000 50  0000 C CNN
F 1 "4.7u" V 6289 4000 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L31.5mm_W9.0mm_P27.50mm_MKS4" H 6488 3850 50  0001 C CNN
F 3 "~" H 6450 4000 50  0001 C CNN
	1    6450 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6600 4000 6850 4000
Wire Wire Line
	7250 3600 7850 3600
Wire Wire Line
	7250 4000 7850 4000
Text Label 7850 4000 2    50   ~ 0
TONE_A
Text Label 7850 3600 2    50   ~ 0
TONE_B
$Comp
L Device:Q_PNP_EBC Q1
U 1 1 5F675A02
P 5550 4300
F 0 "Q1" H 5740 4254 50  0000 L CNN
F 1 "2N2907" H 5740 4345 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-18-3" H 5750 4400 50  0001 C CNN
F 3 "~" H 5550 4300 50  0001 C CNN
	1    5550 4300
	1    0    0    1   
$EndComp
$EndSCHEMATC
